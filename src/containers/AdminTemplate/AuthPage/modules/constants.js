export const AUTH_PAGE_REQUEST = "authPageReducer/AUTH_PAGE_REQUEST";
export const AUTH_PAGE_SUCCESS = "authPageReducer/AUTH_PAGE_SUCCESS";
export const AUTH_PAGE_FAILED = "authPageReducer/AUTH_PAGE_FAILED";

import * as ActionType from "./constants";
import axios from "axios";

export const actloginAuthPage = () => {
  return (dispatch) => {
    axios({
      url: "https://movie0706.cybersoft.edu.vn/api/QuanLyNguoiDung/DangNhap",
      method: "POST",
    })
      .then((result) => {
        console.log(result.data);
        dispatch(actAuthPageSuccess(data));
      })
      .catch((err) => {
        dispatch(actAuthPageFailed(err));
      });
  };
};

const actAuthPageRequest = () => {
  return {
    type: ActionType.LIST_MOVIE_REQUEST,
  };
};

const actAuthPageSuccess = (data) => {
  return {
    type: ActionType.LIST_MOVIE_SUCCESS,
    payload: data,
  };
};

const actAuthPageFailed = (err) => {
  return {
    type: ActionType.LIST_MOVIE_FAILED,
    payload: err,
  };
};
